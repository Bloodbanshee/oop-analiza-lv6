﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_1 = new System.Windows.Forms.Button();
            this.Btn_2 = new System.Windows.Forms.Button();
            this.Btn_3 = new System.Windows.Forms.Button();
            this.Btn_4 = new System.Windows.Forms.Button();
            this.Btn_5 = new System.Windows.Forms.Button();
            this.Btn_6 = new System.Windows.Forms.Button();
            this.Btn_7 = new System.Windows.Forms.Button();
            this.Btn_8 = new System.Windows.Forms.Button();
            this.Btn_9 = new System.Windows.Forms.Button();
            this.Btn_0 = new System.Windows.Forms.Button();
            this.Btn_Decimal_Dot = new System.Windows.Forms.Button();
            this.Btn_Multiply = new System.Windows.Forms.Button();
            this.Btn_Plus = new System.Windows.Forms.Button();
            this.Btn_Minus = new System.Windows.Forms.Button();
            this.Btn_Equal = new System.Windows.Forms.Button();
            this.Btn_Divide = new System.Windows.Forms.Button();
            this.Btn_Delete = new System.Windows.Forms.Button();
            this.Btn_Reset = new System.Windows.Forms.Button();
            this.Btn_Sinus = new System.Windows.Forms.Button();
            this.Btn_Cosinus = new System.Windows.Forms.Button();
            this.Btn_Log = new System.Windows.Forms.Button();
            this.Btn_Tangens = new System.Windows.Forms.Button();
            this.Btn_Root = new System.Windows.Forms.Button();
            this.Screen = new System.Windows.Forms.TextBox();
            this.Label_Input = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Btn_1
            // 
            this.Btn_1.Location = new System.Drawing.Point(12, 260);
            this.Btn_1.Name = "Btn_1";
            this.Btn_1.Size = new System.Drawing.Size(45, 45);
            this.Btn_1.TabIndex = 0;
            this.Btn_1.Text = "1";
            this.Btn_1.UseVisualStyleBackColor = true;
            this.Btn_1.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_2
            // 
            this.Btn_2.Location = new System.Drawing.Point(63, 260);
            this.Btn_2.Name = "Btn_2";
            this.Btn_2.Size = new System.Drawing.Size(45, 45);
            this.Btn_2.TabIndex = 1;
            this.Btn_2.Text = "2";
            this.Btn_2.UseVisualStyleBackColor = true;
            this.Btn_2.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_3
            // 
            this.Btn_3.Location = new System.Drawing.Point(114, 260);
            this.Btn_3.Name = "Btn_3";
            this.Btn_3.Size = new System.Drawing.Size(45, 45);
            this.Btn_3.TabIndex = 2;
            this.Btn_3.Text = "3";
            this.Btn_3.UseVisualStyleBackColor = true;
            this.Btn_3.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_4
            // 
            this.Btn_4.Location = new System.Drawing.Point(12, 212);
            this.Btn_4.Name = "Btn_4";
            this.Btn_4.Size = new System.Drawing.Size(45, 45);
            this.Btn_4.TabIndex = 3;
            this.Btn_4.Text = "4";
            this.Btn_4.UseVisualStyleBackColor = true;
            this.Btn_4.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_5
            // 
            this.Btn_5.Location = new System.Drawing.Point(63, 212);
            this.Btn_5.Name = "Btn_5";
            this.Btn_5.Size = new System.Drawing.Size(45, 45);
            this.Btn_5.TabIndex = 4;
            this.Btn_5.Text = "5";
            this.Btn_5.UseVisualStyleBackColor = true;
            this.Btn_5.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_6
            // 
            this.Btn_6.Location = new System.Drawing.Point(114, 212);
            this.Btn_6.Name = "Btn_6";
            this.Btn_6.Size = new System.Drawing.Size(45, 45);
            this.Btn_6.TabIndex = 5;
            this.Btn_6.Text = "6";
            this.Btn_6.UseVisualStyleBackColor = true;
            this.Btn_6.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_7
            // 
            this.Btn_7.Location = new System.Drawing.Point(12, 161);
            this.Btn_7.Name = "Btn_7";
            this.Btn_7.Size = new System.Drawing.Size(45, 45);
            this.Btn_7.TabIndex = 6;
            this.Btn_7.Text = "7";
            this.Btn_7.UseVisualStyleBackColor = true;
            this.Btn_7.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_8
            // 
            this.Btn_8.Location = new System.Drawing.Point(63, 161);
            this.Btn_8.Name = "Btn_8";
            this.Btn_8.Size = new System.Drawing.Size(45, 45);
            this.Btn_8.TabIndex = 7;
            this.Btn_8.Text = "8";
            this.Btn_8.UseVisualStyleBackColor = true;
            this.Btn_8.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_9
            // 
            this.Btn_9.Location = new System.Drawing.Point(114, 161);
            this.Btn_9.Name = "Btn_9";
            this.Btn_9.Size = new System.Drawing.Size(45, 45);
            this.Btn_9.TabIndex = 8;
            this.Btn_9.Text = "9";
            this.Btn_9.UseVisualStyleBackColor = true;
            this.Btn_9.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_0
            // 
            this.Btn_0.Location = new System.Drawing.Point(12, 311);
            this.Btn_0.Name = "Btn_0";
            this.Btn_0.Size = new System.Drawing.Size(96, 45);
            this.Btn_0.TabIndex = 9;
            this.Btn_0.Text = "0";
            this.Btn_0.UseVisualStyleBackColor = true;
            this.Btn_0.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_Decimal_Dot
            // 
            this.Btn_Decimal_Dot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.Btn_Decimal_Dot.Location = new System.Drawing.Point(114, 311);
            this.Btn_Decimal_Dot.Name = "Btn_Decimal_Dot";
            this.Btn_Decimal_Dot.Size = new System.Drawing.Size(45, 45);
            this.Btn_Decimal_Dot.TabIndex = 10;
            this.Btn_Decimal_Dot.Text = ".";
            this.Btn_Decimal_Dot.UseVisualStyleBackColor = true;
            this.Btn_Decimal_Dot.Click += new System.EventHandler(this.Btn_Click);
            // 
            // Btn_Multiply
            // 
            this.Btn_Multiply.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.Btn_Multiply.Location = new System.Drawing.Point(165, 212);
            this.Btn_Multiply.Name = "Btn_Multiply";
            this.Btn_Multiply.Size = new System.Drawing.Size(45, 45);
            this.Btn_Multiply.TabIndex = 11;
            this.Btn_Multiply.Text = "*";
            this.Btn_Multiply.UseVisualStyleBackColor = true;
            this.Btn_Multiply.Click += new System.EventHandler(this.Btn_Operator_Click);
            // 
            // Btn_Plus
            // 
            this.Btn_Plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.Btn_Plus.Location = new System.Drawing.Point(165, 260);
            this.Btn_Plus.Name = "Btn_Plus";
            this.Btn_Plus.Size = new System.Drawing.Size(45, 45);
            this.Btn_Plus.TabIndex = 13;
            this.Btn_Plus.Text = "+";
            this.Btn_Plus.UseVisualStyleBackColor = true;
            this.Btn_Plus.Click += new System.EventHandler(this.Btn_Operator_Click);
            // 
            // Btn_Minus
            // 
            this.Btn_Minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.Btn_Minus.Location = new System.Drawing.Point(216, 260);
            this.Btn_Minus.Name = "Btn_Minus";
            this.Btn_Minus.Size = new System.Drawing.Size(45, 45);
            this.Btn_Minus.TabIndex = 14;
            this.Btn_Minus.Text = "-";
            this.Btn_Minus.UseVisualStyleBackColor = true;
            this.Btn_Minus.Click += new System.EventHandler(this.Btn_Operator_Click);
            // 
            // Btn_Equal
            // 
            this.Btn_Equal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.Btn_Equal.Location = new System.Drawing.Point(165, 311);
            this.Btn_Equal.Name = "Btn_Equal";
            this.Btn_Equal.Size = new System.Drawing.Size(96, 45);
            this.Btn_Equal.TabIndex = 15;
            this.Btn_Equal.Text = "=";
            this.Btn_Equal.UseVisualStyleBackColor = true;
            this.Btn_Equal.Click += new System.EventHandler(this.Btn_Equal_Click);
            // 
            // Btn_Divide
            // 
            this.Btn_Divide.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.Btn_Divide.Location = new System.Drawing.Point(216, 212);
            this.Btn_Divide.Name = "Btn_Divide";
            this.Btn_Divide.Size = new System.Drawing.Size(45, 45);
            this.Btn_Divide.TabIndex = 16;
            this.Btn_Divide.Text = "/";
            this.Btn_Divide.UseVisualStyleBackColor = true;
            this.Btn_Divide.Click += new System.EventHandler(this.Btn_Operator_Click);
            // 
            // Btn_Delete
            // 
            this.Btn_Delete.BackColor = System.Drawing.SystemColors.Highlight;
            this.Btn_Delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Delete.ForeColor = System.Drawing.SystemColors.Menu;
            this.Btn_Delete.Location = new System.Drawing.Point(165, 161);
            this.Btn_Delete.Name = "Btn_Delete";
            this.Btn_Delete.Size = new System.Drawing.Size(45, 45);
            this.Btn_Delete.TabIndex = 17;
            this.Btn_Delete.Text = "DEL";
            this.Btn_Delete.UseVisualStyleBackColor = false;
            this.Btn_Delete.Click += new System.EventHandler(this.Btn_Delete_Click);
            // 
            // Btn_Reset
            // 
            this.Btn_Reset.BackColor = System.Drawing.SystemColors.Highlight;
            this.Btn_Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.Btn_Reset.ForeColor = System.Drawing.SystemColors.Menu;
            this.Btn_Reset.Location = new System.Drawing.Point(216, 161);
            this.Btn_Reset.Name = "Btn_Reset";
            this.Btn_Reset.Size = new System.Drawing.Size(45, 45);
            this.Btn_Reset.TabIndex = 18;
            this.Btn_Reset.Text = "AC";
            this.Btn_Reset.UseVisualStyleBackColor = false;
            this.Btn_Reset.Click += new System.EventHandler(this.Btn_Reset_Click);
            // 
            // Btn_Sinus
            // 
            this.Btn_Sinus.Location = new System.Drawing.Point(12, 127);
            this.Btn_Sinus.Name = "Btn_Sinus";
            this.Btn_Sinus.Size = new System.Drawing.Size(70, 30);
            this.Btn_Sinus.TabIndex = 19;
            this.Btn_Sinus.Text = "Sin";
            this.Btn_Sinus.UseVisualStyleBackColor = true;
            this.Btn_Sinus.Click += new System.EventHandler(this.Btn_Sinus_Click);
            // 
            // Btn_Cosinus
            // 
            this.Btn_Cosinus.Location = new System.Drawing.Point(102, 127);
            this.Btn_Cosinus.Name = "Btn_Cosinus";
            this.Btn_Cosinus.Size = new System.Drawing.Size(70, 30);
            this.Btn_Cosinus.TabIndex = 20;
            this.Btn_Cosinus.Text = "Cos";
            this.Btn_Cosinus.UseVisualStyleBackColor = true;
            this.Btn_Cosinus.Click += new System.EventHandler(this.Btn_Cosinus_Click);
            // 
            // Btn_Log
            // 
            this.Btn_Log.Location = new System.Drawing.Point(42, 92);
            this.Btn_Log.Name = "Btn_Log";
            this.Btn_Log.Size = new System.Drawing.Size(70, 30);
            this.Btn_Log.TabIndex = 21;
            this.Btn_Log.Text = "Log";
            this.Btn_Log.UseVisualStyleBackColor = true;
            this.Btn_Log.Click += new System.EventHandler(this.Btn_Log_Click);
            // 
            // Btn_Tangens
            // 
            this.Btn_Tangens.Location = new System.Drawing.Point(191, 127);
            this.Btn_Tangens.Name = "Btn_Tangens";
            this.Btn_Tangens.Size = new System.Drawing.Size(70, 30);
            this.Btn_Tangens.TabIndex = 22;
            this.Btn_Tangens.Text = "Tan";
            this.Btn_Tangens.UseVisualStyleBackColor = true;
            this.Btn_Tangens.Click += new System.EventHandler(this.Btn_Tangens_Click);
            // 
            // Btn_Root
            // 
            this.Btn_Root.Location = new System.Drawing.Point(149, 92);
            this.Btn_Root.Name = "Btn_Root";
            this.Btn_Root.Size = new System.Drawing.Size(70, 30);
            this.Btn_Root.TabIndex = 23;
            this.Btn_Root.Text = "Sqrt";
            this.Btn_Root.UseVisualStyleBackColor = true;
            this.Btn_Root.Click += new System.EventHandler(this.Btn_Root_Click);
            // 
            // Screen
            // 
            this.Screen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.Screen.Location = new System.Drawing.Point(12, 60);
            this.Screen.Name = "Screen";
            this.Screen.ReadOnly = true;
            this.Screen.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Screen.Size = new System.Drawing.Size(249, 26);
            this.Screen.TabIndex = 24;
            this.Screen.Text = "0";
            // 
            // Label_Input
            // 
            this.Label_Input.AutoSize = true;
            this.Label_Input.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Input.Location = new System.Drawing.Point(12, 41);
            this.Label_Input.Name = "Label_Input";
            this.Label_Input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label_Input.Size = new System.Drawing.Size(0, 16);
            this.Label_Input.TabIndex = 25;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 361);
            this.Controls.Add(this.Label_Input);
            this.Controls.Add(this.Screen);
            this.Controls.Add(this.Btn_Root);
            this.Controls.Add(this.Btn_Tangens);
            this.Controls.Add(this.Btn_Log);
            this.Controls.Add(this.Btn_Cosinus);
            this.Controls.Add(this.Btn_Sinus);
            this.Controls.Add(this.Btn_Reset);
            this.Controls.Add(this.Btn_Delete);
            this.Controls.Add(this.Btn_Divide);
            this.Controls.Add(this.Btn_Equal);
            this.Controls.Add(this.Btn_Minus);
            this.Controls.Add(this.Btn_Plus);
            this.Controls.Add(this.Btn_Multiply);
            this.Controls.Add(this.Btn_Decimal_Dot);
            this.Controls.Add(this.Btn_0);
            this.Controls.Add(this.Btn_9);
            this.Controls.Add(this.Btn_8);
            this.Controls.Add(this.Btn_7);
            this.Controls.Add(this.Btn_6);
            this.Controls.Add(this.Btn_5);
            this.Controls.Add(this.Btn_4);
            this.Controls.Add(this.Btn_3);
            this.Controls.Add(this.Btn_2);
            this.Controls.Add(this.Btn_1);
            this.Name = "Form1";
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_1;
        private System.Windows.Forms.Button Btn_2;
        private System.Windows.Forms.Button Btn_3;
        private System.Windows.Forms.Button Btn_4;
        private System.Windows.Forms.Button Btn_5;
        private System.Windows.Forms.Button Btn_6;
        private System.Windows.Forms.Button Btn_7;
        private System.Windows.Forms.Button Btn_8;
        private System.Windows.Forms.Button Btn_9;
        private System.Windows.Forms.Button Btn_0;
        private System.Windows.Forms.Button Btn_Decimal_Dot;
        private System.Windows.Forms.Button Btn_Multiply;
        private System.Windows.Forms.Button Btn_Plus;
        private System.Windows.Forms.Button Btn_Minus;
        private System.Windows.Forms.Button Btn_Equal;
        private System.Windows.Forms.Button Btn_Divide;
        private System.Windows.Forms.Button Btn_Delete;
        private System.Windows.Forms.Button Btn_Reset;
        private System.Windows.Forms.Button Btn_Sinus;
        private System.Windows.Forms.Button Btn_Cosinus;
        private System.Windows.Forms.Button Btn_Log;
        private System.Windows.Forms.Button Btn_Tangens;
        private System.Windows.Forms.Button Btn_Root;
        private System.Windows.Forms.TextBox Screen;
        private System.Windows.Forms.Label Label_Input;
    }
}

