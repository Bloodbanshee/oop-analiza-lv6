﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        double resultComponent = 0;
        string operationPerformed = "";
        bool isOperationPerformed = false;
        bool isAdvancedOperationPerformed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            if ((Screen.Text == "0") || (isOperationPerformed) || (isAdvancedOperationPerformed)){
                Screen.Clear();
            }
            isOperationPerformed = false;
            isAdvancedOperationPerformed = false;
            Button button = (Button)sender;
            if(button.Text == ".")
            {
                if(Screen.Text.Contains("."))
                    Screen.Text += button.Text;
            }
            else 
            Screen.Text += button.Text;
        }

        private void Btn_Delete_Click(object sender, EventArgs e)
        {
            Screen.Text = "0";
        }

        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            Screen.Text = "0";
            resultComponent = 0;
        }

        private void Btn_Operator_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            operationPerformed = button.Text;
            resultComponent = double.Parse(Screen.Text);
            Label_Input.Text = resultComponent + " " + operationPerformed;
            isOperationPerformed = true;
        }

        private void Btn_Equal_Click(object sender, EventArgs e)
        {
            switch (operationPerformed)
            {
                case "+":
                    Screen.Text = (resultComponent + double.Parse(Screen.Text)).ToString();
                    break;
                case "-":
                    Screen.Text = (resultComponent - double.Parse(Screen.Text)).ToString();
                    break;
                case "*":
                    Screen.Text = (resultComponent * double.Parse(Screen.Text)).ToString();
                    break;
                case "/":
                    Screen.Text = (resultComponent / double.Parse(Screen.Text)).ToString();
                    break;
                default:
                    break;
            }
        }

        private void Btn_Sinus_Click(object sender, EventArgs e)
        {
            resultComponent = Convert.ToDouble(Screen.Text);
            double Result = 0;
            Result = Math.Sin(resultComponent);
            Screen.Text = Result.ToString();
            isAdvancedOperationPerformed = true;
        }

        private void Btn_Cosinus_Click(object sender, EventArgs e)
        {
            resultComponent = Convert.ToDouble(Screen.Text);
            double Result = 0;
            Result = Math.Cos(resultComponent);
            Screen.Text = Result.ToString();
            isAdvancedOperationPerformed = true;
        }

        private void Btn_Tangens_Click(object sender, EventArgs e)
        {
            resultComponent = Convert.ToDouble(Screen.Text);
            double Result = 0;
            Result = Math.Tan(resultComponent);
            Screen.Text = Result.ToString();
            isAdvancedOperationPerformed = true;
        }

        private void Btn_Log_Click(object sender, EventArgs e)
        {
            resultComponent = Convert.ToDouble(Screen.Text);
            double Result;
            Result = Math.Log(resultComponent);
            Screen.Text = Result.ToString();
            isAdvancedOperationPerformed = true;
        }

        private void Btn_Root_Click(object sender, EventArgs e)
        {
            resultComponent = Convert.ToDouble(Screen.Text);
            double Result = 0;
            Result = Math.Sqrt(resultComponent);
            Screen.Text = Result.ToString();
            isAdvancedOperationPerformed = true;
        }
    }
}
