﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analize_LV7
{
   
    public partial class Form1 : Form
    {
        bool turn = true; // true = X turn; flase = O turn
        public Form1()
        {
            InitializeComponent();
        }

        private void NewGame()
        {
            label_turn.Text = "Potez: " + textBox_P1.Text;

            button1.Text = "";
            button2.Text = "";
            button3.Text = "";
            button4.Text = "";
            button5.Text = "";
            button6.Text = "";
            button7.Text = "";
            button8.Text = "";
            button9.Text = "";

            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;
        }

        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn)
            {
                label_turn.Text = "Turn: " + textBox_P2.Text;
                b.Text = "X";
            }
            else
            {
                label_turn.Text = "Turn: " + textBox_P1.Text;
                b.Text = "O";
            }

            turn = !turn;
            b.Enabled = false;

            if ((button1.Text == "X" && button2.Text == "X" && button3.Text == "X") || (button4.Text == "X" && button5.Text == "X" && button6.Text == "X") || (button7.Text == "X" && button8.Text == "X" && button9.Text == "X"))
            {
                MessageBox.Show(textBox_P1.Text + " Pobjeda!");
                NewGame();
            }
            else if ((button1.Text == "O" && button2.Text == "O" && button3.Text == "O") || (button4.Text == "O" && button5.Text == "O" && button6.Text == "O") || (button7.Text == "O" && button8.Text == "O" && button9.Text == "O"))
            {
                MessageBox.Show(textBox_P2.Text + " Pobjeda!");
                NewGame();
            }
            else if ((button1.Text == "X" && button4.Text == "X" && button7.Text == "X") || (button2.Text == "X" && button5.Text == "X" && button8.Text == "X") || (button3.Text == "X" && button6.Text == "X" && button9.Text == "X"))
            {
                MessageBox.Show(textBox_P1.Text + " Pobjeda!");
                NewGame();
            }
            else if ((button1.Text == "O" && button4.Text == "O" && button7.Text == "O") || (button2.Text == "O" && button5.Text == "O" && button8.Text == "O") || (button3.Text == "O" && button6.Text == "O" && button9.Text == "O"))
            {
                MessageBox.Show(textBox_P2.Text + " Pobjeda!");
                NewGame();
            }
            else if ((button1.Text == "X" && button5.Text == "X" && button9.Text == "X") || (button3.Text == "X" && button5.Text == "X" && button7.Text == "X"))
            {
                MessageBox.Show(textBox_P1.Text + " Pobjeda!");
                NewGame();
            }
            else if ((button1.Text == "O" && button5.Text == "O" && button9.Text == "O") || (button3.Text == "O" && button5.Text == "O" && button7.Text == "O"))
            {
                MessageBox.Show(textBox_P2.Text + " Pobjeda!");
                NewGame();
            }
            else
            {
                if (!button1.Enabled && !button2.Enabled && !button3.Enabled && !button4.Enabled && !button5.Enabled && !button6.Enabled && !button7.Enabled && !button8.Enabled && !button9.Enabled)
                {
                    MessageBox.Show("Izjednacenje!");
                    NewGame();
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            if (textBox_P1.Text.Length == 0 || (textBox_P2.Text.Length == 0))
            {
                MessageBox.Show("Invalid player name!");
            }
            else
            {
                NewGame();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = false;
            button8.Enabled = false;
            button9.Enabled = false;
        }

    }

}
