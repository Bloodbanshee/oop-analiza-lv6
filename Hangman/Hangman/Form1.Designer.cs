﻿namespace Hangman
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Guess = new System.Windows.Forms.Button();
            this.label_Guess = new System.Windows.Forms.Label();
            this.textBox_guess = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.label_attempts = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Btn_Guess
            // 
            this.Btn_Guess.Location = new System.Drawing.Point(20, 226);
            this.Btn_Guess.Name = "Btn_Guess";
            this.Btn_Guess.Size = new System.Drawing.Size(140, 70);
            this.Btn_Guess.TabIndex = 2;
            this.Btn_Guess.Text = "Guess";
            this.Btn_Guess.UseVisualStyleBackColor = true;
            this.Btn_Guess.Click += new System.EventHandler(this.Btn_Guess_Click);
            // 
            // label_Guess
            // 
            this.label_Guess.AutoSize = true;
            this.label_Guess.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Guess.Location = new System.Drawing.Point(12, 170);
            this.label_Guess.Name = "label_Guess";
            this.label_Guess.Size = new System.Drawing.Size(200, 31);
            this.label_Guess.TabIndex = 3;
            this.label_Guess.Text = "Guess a Letter:";
            // 
            // textBox_guess
            // 
            this.textBox_guess.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_guess.Location = new System.Drawing.Point(218, 170);
            this.textBox_guess.Name = "textBox_guess";
            this.textBox_guess.Size = new System.Drawing.Size(35, 31);
            this.textBox_guess.TabIndex = 4;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(453, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // label_attempts
            // 
            this.label_attempts.AutoSize = true;
            this.label_attempts.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_attempts.Location = new System.Drawing.Point(29, 331);
            this.label_attempts.Name = "label_attempts";
            this.label_attempts.Size = new System.Drawing.Size(86, 31);
            this.label_attempts.TabIndex = 6;
            this.label_attempts.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 450);
            this.Controls.Add(this.label_attempts);
            this.Controls.Add(this.textBox_guess);
            this.Controls.Add(this.label_Guess);
            this.Controls.Add(this.Btn_Guess);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Hangman";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Btn_Guess;
        private System.Windows.Forms.Label label_Guess;
        private System.Windows.Forms.TextBox textBox_guess;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label label_attempts;
    }
}

