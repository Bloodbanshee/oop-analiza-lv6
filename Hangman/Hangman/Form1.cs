﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hangman
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int hit = 0;
        int pokusaji = 8;
        string word;
        static Random rnd = new Random();

        List<Label> labels = new List<Label>();
        List<string> wordlist = new List<string>();
        string path = "C:\\Users\\danij\\Documents\\wordlist.txt";

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    wordlist.Add(line);
                }
            }

            int r = rnd.Next(wordlist.Count);
            word = (string)wordlist[r];

            labels = new List<Label>();
            int startX = 80;
            foreach (char c in word)
            {
                Label lbl = new Label();
                lbl.Text = "_";
                lbl.Font = new Font(lbl.Font.Name, 20, lbl.Font.Style);
                lbl.Location = new Point(startX, 90);
                lbl.Tag = c.ToString();
                lbl.AutoSize = true;
                this.Controls.Add(lbl);
                labels.Add(lbl);
                startX = lbl.Right;
            }
        }

        private void Btn_Guess_Click(object sender, EventArgs e)
        {
            if (textBox_guess.Text.Length == 0 || textBox_guess.Text.Length > 1)
            {
                textBox_guess.Text = "";
                MessageBox.Show("Pogrešan unos!", "Greška!");
            }
            else
            {
                if (word.Contains(textBox_guess.Text))
                {
                    for (int i = 0; i < word.Length; i++)
                    {
                        if (word.IndexOf(textBox_guess.Text) == word.IndexOf(word[i]))
                        {
                            if (labels[i].Text == "_")
                            {
                                labels[i].Text = textBox_guess.Text;
                                hit++;
                            }
                            else
                            {
                                MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                break;
                            }
                        }
                    }
                    textBox_guess.Text = "";
                    if (hit == word.Length)
                    {
                        MessageBox.Show("Pobjedio si!", "Pobjeda!");
                        Application.Exit();
                    }
                }
                else
                {
                    if (pokusaji == 1)
                    {
                        textBox_guess.Text = "";
                        pokusaji--;
                        label_attempts.Text = "Broj preostalih pokusaja: " + pokusaji;
                        MessageBox.Show("Izgubio si!", "Poraz!");
                        Application.Exit();
                    }
                    else
                    {
                        textBox_guess.Text = "";
                        pokusaji--;
                        label_attempts.Text = "Broj preostalih pokusaja: " + pokusaji;
                    }
                }
            }
        }
    }
}